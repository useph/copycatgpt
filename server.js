const express = require("express");
const { Configuration, OpenAIApi } = require("openai");
const mongoose = require("mongoose");
require("dotenv").config();
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const uri = "mongodb://mongo/mydb";
// const uri = "mongodb://localhost/mydb";
mongoose.set("strictQuery", true);
mongoose.connect(uri, {
	family: 4,
	useNewUrlParser: true,
});

const historySchema = new mongoose.Schema({
	authorization: {
		type: String,
		require: true,
	},
	conversation: [
		{
			userInput: {
				type: String,
				required: true,
			},
			chatGPTResponse: {
				type: String,
				required: true,
			},
		},
	],
});

historySchema.methods.getHistory = function() {
	let history =
		"The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.";

	for (let QandA of this.conversation) {
		history += `

Question: ${QandA.userInput}
Answer: ${QandA.chatGPTResponse}
`;
	}

	return history;
};

const ChatHistory = mongoose.model("ChatHistory", historySchema);

app.post("/api/chatAI", async (req, res) => {
	const { prompt } = req.body;
	const apiKey = req.headers["authorization"];

	let doc;
	try {
		doc = await ChatHistory.findOne({ authorization: apiKey });
		if (!doc) {
			doc = await ChatHistory.create({ authorization: apiKey });
		}
	} catch (e) {
		console.error(e);
	}
	const openai = new OpenAIApi(new Configuration({ apiKey }));
	let response;
	try {
		response = await openai.createCompletion({
			model: "text-davinci-003",
			prompt: `
${doc.getHistory()}


Question: ${prompt}
Answer:
`,
			max_tokens: 300,
			temperature: 0.7,
		});

		const { text: chatGPTResponse } = response.data.choices[0];

		doc.conversation.push({
			userInput: prompt,
			chatGPTResponse: chatGPTResponse,
		});

		try {
			await doc.save();
		} catch (e) {
			console.error(e);
		}

		res.json({ message: chatGPTResponse });
	} catch (e) {
		console.log({e});
		const { status, data: { error } } = e.response;
		res.status(status).json({ error: { message: error.message } });
	}

});

app.get("/api/chatAI", async (req, res) => {
	const apiKey = req.headers["authorization"];
	console.log({ apiKey });

	let doc;
	try {
		doc = await ChatHistory.findOne({ authorization: apiKey });
		if (!doc) {
			ChatHistory.create({ authorization: apiKey });
			console.log({ doc });
		}
	} catch (e) {
		console.error(e);
	}
	res.json({ conversation: doc?.conversation ?? [] });
});

app.get("/", (_, res) => {
	res.send(`Server is running on port ${PORT}`);
});

const PORT = process.env.PORT || 6969;
const db = mongoose.connection;
db.once("open", () => {
	console.log("Connected to Database");
	app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
});
db.on("error", (error) => console.error(error));
