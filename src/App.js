import { useEffect, useState } from "react";
import "./App.css";

const URL = "http://localhost:6969/api/chatAI";

function App() {
	const [config, setConfig] = useState({});
	const [value, setValue] = useState("");
	const [prompt, setPrompt] = useState("");
	const [conversation, setConversation] = useState([]);

	useEffect(() => {
		const getConversation = async () => {
			const res = await fetch(URL, {
				method: "get",
				headers: {
					"Content-Type": "application/json",
					Authorization: config.apiKey,
				},
			});
			const data = await res.json();
			console.log({ data });
			setConversation(data.conversation);
		};

		getConversation();
	}, [config]);

	const handleSubmit = async (e) => {
		e.preventDefault();
		let res;
		if (!config.apiKey) {
				setConversation([
					...conversation,
					{ userInput: prompt, chatGPTResponse: "Please provide an API key and click Sumbit before asking a question" },
				]);
			return;
		}
		try {
			res = await fetch(URL, {
				method: "post",
				headers: {
					"Content-Type": "application/json",
					Authorization: config.apiKey,
				},
				body: JSON.stringify({ prompt }),
			});
			const data = await res.json();
			if (data.error) {
				setConversation([
					...conversation,
					{ userInput: prompt, chatGPTResponse: data.error.message },
				]);
			} else {
				setConversation([
					...conversation,
					{ userInput: prompt, chatGPTResponse: data.message },
				]);
			}
			setPrompt("");
		} catch (e) {
			console.error(e);
		}
	};

	const handleChange = (e) => {
		const { value } = e.target;
		setPrompt(value);
	};

	return (
		<div className="App">
			<main>
				<div className="config">
					<form
						onSubmit={(e) => {
							e.preventDefault();
							setConfig({ ...config, apiKey: value });
						}}
					>
						<label htmlFor="api">api  </label>
						<input
							id="api"
							value={value}
							onChange={({ target }) => setValue(target.value)}
							name="api"
							placeholder="Enter you're API key here"
							type="text"
						/>
						<input value="login" type="submit"/>
						<button onClick={() => {
							setConfig({});
							setConversation([]);
							setValue("");
						}}>logout</button>
					</form>
				</div>
				<div className="chat">
					<section className="chat__history">
						{conversation.map((chat) => {
							return (
								<>
									<div>{chat.userInput}</div>
									<div>{chat.chatGPTResponse}</div>
									<br />
								</>
							);
						})}
					</section>
					<section className="chat__input">
						<form onSubmit={handleSubmit}>
							<div className="prompt-group">
								<textarea
									className="prompt"
									value={prompt}
									onChange={handleChange}
								></textarea>
								<button className="btn" type="submit">
									submit
								</button>
							</div>
						</form>
					</section>
				</div>
			</main>
		</div>
	);
}

export default App;
